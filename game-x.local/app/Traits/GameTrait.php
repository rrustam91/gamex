<?php
namespace App\Traits;

use App\Models\TowerGames;
use Illuminate\Support\Arr;

trait GameTrait {


    public function checkSelection(int $selection)
    {
        return  in_array($selection , $this->area[$this->step]);
    }

    public function nextStep($selection)
    {
        $this->updateState($selection);
        (!$this->isLastStep()) ?  self::increment('step') : $this->finishedGame();
        $this->update();
    }

    public function finishedGame()
    {
        $this->finishedState();
        $this->step = 10;
        $this->status = TowerGames::STATUS_WINNED;
        $this->update();
    }

    public function loseGame($selection){
        $this->loseState($selection);
        $this->status = TowerGames::STATUS_LOSED;
        $this->update();
    }


    public function isLastStep() : bool
    {
        return $this->step >= TowerGames::MAX_STEP;
    }

    protected function updateState( $selection ){
        $state = json_decode($this->state);

        $state->coeff = $this->getCoeff($this->count, $this->step);
        $state->count = $this->count;
        $state->profit = $this->amount * $state->coeff;
        $state->field[$this->step] = $this->area[$this->step];
        $state->revealed[$this->step] = $selection;

        $this->state = json_encode($state);
    }

    protected function finishedState(){
        $state = json_decode($this->state);

        $state->coeff = $this->getCoeff($this->count, $this->step);
        $state->count = $this->count;
        $state->profit = $this->amount * $state->coeff;
        $state->field = $this->area;

        $this->state = json_encode($state);
    }


    protected function loseState($selection){
        $state = json_decode($this->state);

        $state->coeff = $this->getCoeff($this->count, $this->step);
        $state->count = $this->count;
        $state->profit = $this->amount * $state->coeff;
        $state->field = $this->area;
        $state->revealed[$this->step] = $selection;

        $this->state = json_encode($state);
    }


    protected function getCoeff($bombs = 1, $step = 0){
        $fields = 5;
        $ratio = 1;
        $diamonds = $fields - $bombs;
        for($i = 0;  $i < $step+1; $i++){
            $ratio *= ($diamonds / $fields);
            $result = $this->round(0.95 / $ratio, 2);
        }
        return $result;
    }

    protected static function round($val, $precision) {
       $k = pow(10, $precision);
       return round($val * $k) / $k;
    }
}
