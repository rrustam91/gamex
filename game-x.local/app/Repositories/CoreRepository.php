<?php
namespace App\Repositories;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoreRepository
 *
 * @package App\Repositories
 *
 * Репозиторий работы с сущностью
 */

abstract class CoreRepository {


    /**
     * @var Model
     */
    protected $model;

    /**
     * CoreRepository contructor
    */
    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     *  @return Model | Illuminate\Foundation\Application | mixed
     */
    protected function startConditions()
    {
        return clone $this->model;
    }


    /**
     * Получение всех записей модели
     * @return Model
     */
    public function getAll()
    {
        return $this->startConditions()->get();
    }


    /**
     * Получение модели для редактирования
     * @param int $id
     * @return Model
     */
    public function getById($id)
    {
        return $this->startConditions()->find($id);
    }


    public function getStatuses()
    {
        return AppModel::STATUSES;
    }


}
