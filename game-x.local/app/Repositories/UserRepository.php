<?php

namespace App\Repositories;

use App\Repositories\CoreRepository;
use App\Models\User as Model;

use Illuminate\Database\Eloquent\Collection;

class UsersRepository extends CoreRepository
{
    /**
     * @return string
    */
    protected function getModelClass(){
        return Model::class;
    }

}
