<?php

namespace App\Repositories;

use App\Repositories\CoreRepository;
use App\Models\TowerGames as Model;

use Illuminate\Database\Eloquent\Collection;

class GameRepository extends CoreRepository
{
    /**
     * @return string
    */
    protected function getModelClass(){
        return Model::class;
    }

    public function getAuthors(){
        return $this->startConditions()->get();
    }

}
