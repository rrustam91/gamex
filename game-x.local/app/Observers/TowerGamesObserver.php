<?php

namespace App\Observers;

use App\Models\TowerGames;
use Illuminate\Support\Arr;

use Illuminate\Support\Str;

class TowerGamesObserver
{

    /**
     * Handle the TowerGames "creating" event.
     *
     * @return void
     */
    public function creating(TowerGames $model)
    {
        $this->setUser($model);
        $this->setArea($model);
        $this->setState($model);
        $model->status = TowerGames::STATUS_ACTIVE;
    }
    /**
     * Handle the TowerGames "created" event.
     *
     * @return void
     */
    public function created(TowerGames $model)
    {

    }

    /**
     * Handle the TowerGames "updating" event.
     * @return void
     */
    public function updating(TowerGames $model)
    {
        //dd('updating', $model->selection);
    }


    /**
     * Handle the TowerGames "updated" event.
     *
     * @return void
     */
    public function updated(TowerGames $model)
    {
        //dd($model->selection);
    }

    /**
     * Handle the TowerGames "deleted" event.
     *
     * @return void
     */
    public function deleted(TowerGames $model)
    {
        //
    }

    /**
     * Handle the TowerGames "restored" event.
     *
     * @return void
     */
    public function restored(TowerGames $model)
    {
        //
    }

    /**
     * Handle the TowerGames "force deleted" event.
     *
     * @return void
     */
    public function forceDeleted(TowerGames $model)
    {
        //
    }


    protected function setUser(TowerGames $model){
        if (empty($model->user_id) || $model->isDirty('user_id')) {
            $model->user_id = auth()->id()  ;
        }
    }


    protected function setArea(TowerGames $model){

        $count = ($model->count <= 0) ? 1 : $model->count;
        if (empty($model->area) || $model->isDirty('area')) {
            for($i = 0; $i < 10; $i ++){
                $area[] = Arr::random([0, 1, 2, 3, 4], $count);
            }
            $model->area =  $area;
        }
    }
    protected function setState(TowerGames $model){
        $state = [
            'coeff' => 1,
            'count' => $model->count,
            'field' => [],
            'profit' => 0,
            'revealed' => []
        ];
        $model->state = json_encode($state);
    }



}
