<?php

namespace App\Http\Controllers;

use App\Models\TowerGames;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:api');
    }
/**
     * Профиль юзера
     * @return
     */
    public function index() {
        return response()->json($this->guard()->user());
    }


    public function lastGame(){
        $game = $this->guard()->user()->lastGame;
        return response()->json(['game' =>
            ($game && $game->isActive()) ? $game : null
        ]);
    }


    public function history()
    {
        return response()->json($this->guard()->user()->historyGames);
    }

    public function balance()
    {
        return response()->json($this->guard()->user()->balance);
    }

}
