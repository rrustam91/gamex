<?php
namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\TowerGames;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;


class AuthController extends Controller
{
    /**
     * Конструктор с мидлваром
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Авторизация и отправка токена при успешной авторизации
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request){
        $credentials = $request->only('login', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }


        return response()->json(['success'=>false, 'msg' => 'Не правильный логин или пароль '], 401);

    }

    /**
     * Регистрация пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegistrationRequest $request) {

        $user = User::create( [
            'login'=>$request->login,
            'password' => bcrypt($request->password)
       ]);

        if($user){
            $credentials = $request->only('login', 'password');
            $token = $this->guard()->attempt($credentials);
            return response()->json([
                'success'=>true,
                'user' => $user,
                'access_token' => $token,
                'token_type' => 'Bearer',
                'expires_in' => $this->guard()->factory()->getTTL() * 60,
            ], 201);
        }

    }


    /**
     * Выход
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'Logout']);
    }

    /**
     * Refresh токен.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->respondWithToken($this->guard()->refresh());
    }





    /**
     * Генерация нового токена
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'success' => true,
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
        ]);
    }



}
