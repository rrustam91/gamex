<?php

namespace App\Http\Controllers;

use App\Http\Requests\TowerGame\MoveRequest;
use App\Http\Requests\TowerGame\StartRequest;
use App\Http\Requests\TowerGameRequest;
use App\Models\TowerGames;
use App\Repositories\GameRepository;
use Illuminate\Support\Str;

use App\Helpers\CoeffHelper;
use App\Http\Requests\TowerGame\FinishRequest;

class GameController extends Controller
{
    private $repository;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->repository = app(GameRepository::class);
    }

    public function start(StartRequest $request)
    {
        $game = TowerGames::create([
            'count'=> $request->count,
            'amount' => $request->amount,
            'code' => md5(Str::random(40))
        ]);

        return response()->json([
            'game' => $game,
        ]);
    }

    public function move(MoveRequest $request)
    {
        if($game = TowerGames::firstWhere('code', $request->code)){

            if(!$game->checkSelection($request->selection)){
                $game->nextStep($request->selection);
            }else{
                $game->loseGame($request->selection);
            }

            return response()->json(['game' => $game]);
        }
        return response()->json([ 'error'=>'Game not found' ], 404);

    }

    public function finish(FinishRequest $request)
    {
        if($game = TowerGames::firstWhere('code', $request->code)){
            $game->finishedGame();
            return response()->json(['game' => $game]);
        }
    }
    /*

    public function status(TowerGameRequest $request)
    {

    }

    public function history(TowerGameRequest $request)
    {

    }
    */
}
