<?php

namespace App\Http\Requests\TowerGame;

use App\Http\Requests\AppRequest;

class StartRequest extends AppRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count'=>'required|integer|between:1,4|min:1',
            'amount' => 'required|integer'
        ];
    }
}
