<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\AppRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends AppRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'login' => 'required|min:3',
            'password' => 'required|min:3',
        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Введите логин',
            'login.min' => 'Логин должен содержать мин. 3 символа',
            'password.required' => 'Введите пароль',
            'password.min' => 'Пароль должен содержать мин. 3 символа',
        ];

    }

}
