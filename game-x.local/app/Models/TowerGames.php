<?php

namespace App\Models;

use App\Observers\TowerGamesObserver;
use App\Traits\GameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class TowerGames extends Model
{
    use HasFactory, GameTrait;
    protected $table = "tower_games";

    protected $visible = ['id','state', 'amount' , 'code', 'status', 'count', 'profit', 'step'];

    protected $primaryKey = 'id';
    protected $guarded = [
        'id',
        'area',
        'step',
        'status',
        'profit'
    ];
    protected $fillable = [
        'amount',
        'count',
        'code',

    ];

    protected $casts = [
        'area' => 'array',
        'state'=>'array'
    ];

    const STATUS_ACTIVE = 10;
    const STATUS_STARTED = 2;
    const STATUS_FINISHED = 3;
    const STATUS_LOSED = 4;
    const STATUS_WINNED = 5;
    const MAX_STEP = 9;


    public static function boot()
    {
        parent::boot();
        TowerGames::observe(TowerGamesObserver::class);
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function isActive(){
        return $this->status === self::STATUS_ACTIVE;
    }




}
