<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppModel extends Model
{
    use HasFactory;


    const STATUS_ACTIVE = 1;
    const STATUS_STARTED = 2;
    const STATUS_FINISHED = 3;
    const STATUS_LOSED = 4;
    const STATUS_WINNED = 5;

    public const STATUSES = [
        'Активная' => self::STATUS_ACTIVE,
        'Запущено' => self::STATUS_STARTED,
        'Закончено' => self::STATUS_FINISHED,
        'Проигрыш' => self::STATUS_LOSED,
        'Выйгрыш' => self::STATUS_WINNED,
    ];

}
