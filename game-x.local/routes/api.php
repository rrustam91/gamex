<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\ProfileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
});


Route::group([
    'middleware' => 'api',
    'prefix' => 'profile'

], function ($router) {
    Route::get('/', [ProfileController::class, 'index']);
    Route::get('/last-game', [ProfileController::class, 'lastGame']);
    Route::get('/history', [ProfileController::class, 'history']);
    Route::get('/balance', [ProfileController::class, 'balance']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'game'

], function ($router) {
    Route::post('/start', [GameController::class, 'start']);
    Route::post('/move', [GameController::class, 'move']);

    Route::post('/finish', [GameController::class, 'finish']);
     /*Route::post('/status', [GameController::class, 'status']);
    Route::get('/history', [GameController::class, 'history']);
    */
});
