export default function(bombs = 1){

  let lines = 10;
  let fieldsPerLine = 5;
  let bombsPerLine = bombs;
  let diamondsPerLine = fieldsPerLine - bombsPerLine;
  let result = []

  let round = (value, precision = 2) => {
    let k = Math.pow(10, precision);
    return Math.round(value * k) / k;
  };
  for (let line = 1, ratio = 1; line <= lines; line++) {
    ratio *= (diamondsPerLine / fieldsPerLine);
    let rate = round(0.95 / ratio, 2);
    result.push(rate);
  }
  return result
}
