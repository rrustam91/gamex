export const state = () => ({
  errors: []
})

export const mutations = {

}

export const actions = {

  nuxtServerInit ({ commit, dispatch, getters  }, { req }) {

  },
  setErrors(state, err){
    state.errors.push({
      err
    })
  },
  history( {getters} ){
    try {
      const result = this.$axios.get('api/profile/history')
          .then( ({data})  => {
            return data
          })
				  .catch( (error) => {
            return error
          })
          return result
    }catch (e){
      console.log('catch' , e.response)
    }
  }

}

export const getters = {

  getErrors: state => {
    return state.errors
  }
}
