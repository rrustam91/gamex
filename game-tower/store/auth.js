import AuthEndpoints from '@/config/AuthEndpoints.js'
import * as cookie from '@/plugins/cookie.js'

export const state = () => ({
  isLoggedIn: Boolean(cookie.getItem('token')),
  token: cookie.getItem('token') || null,
  refresh_token: null,
	user: null,
  token_type: 'Bearer',
  balance: 1360043
})

export const mutations = {
  SET_TOKEN (state, token) {
		state.token = token
  },


  SET_LOGGEDIN (state) {
    state.isLoggedIn = Boolean(state.token)
  },

  LOGOUT (state) {
    state.isLoggedIn = null
    state.token = null
    cookie.deleteItem('token')
  },
  SET_USER (state, user) {
    state.user = user
  }
}

export const actions = {

	async login({ commit, dispatch }, payload) {
   try {
       const auth = await this.$axios.post('api/auth/login', payload)
          .then( ({data}) =>  {
              if (data.success) {
                const token = data.access_token
                const token_type = data.token_type
                const expire = data.expire;

                commit('SET_TOKEN', token)
                cookie.setItem('token', token, { path: '', maxAge: expire })


                this.$axios.setToken(token, token_type)
                commit('SET_LOGGEDIN')

              }
              return data
           })
          .catch( ({ response })  => {
            return response.data
          });
          return auth

      }catch( err ) {
        return {error: true, err};
      }

    },

  /// Регистрация
  async registration ({ commit, dispatch }, payload) {
    try {
      const auth = await this.$axios.post('api/auth/register', payload)
         .then( ({data}) =>  {
             if (data.success) {
               const token = data.access_token
               const token_type = data.token_type
               const expire = data.expire;

               commit('SET_TOKEN', token)
               cookie.setItem('token', token, { path: '', maxAge: expire })


               this.$axios.setToken(token, token_type)
               commit('SET_LOGGEDIN')

             }
             return data
          })
         .catch( ({ response })  => {
           return response.data
         });
         return auth

     }catch( err ) {
       return {error: true, err};
     }

   },

  /// Сброс пароля
  reset_password ({ commit, dispatch }, formData) {
    return true
  },

  /// Выход
  logout ({ commit, dispatch }) {
		commit('LOGOUT')
		return true
  },

	async refresh_token  ({ commit, dispatch }, formData) {
		try {
			const login = await this.$axios.post(AuthEndpoints.auth.refresh, formData)
				.then( ({status, data})  => {

					if (status.toString() === '200') {
						const token = data.token
						const refresh_token = data.refresh_token
						this.$axios.setToken(token, 'Bearer')
						localStorage.setItem('token', token)

						commit('SET_TOKEN', token)
						commit('SET_REFRESH_TOKEN', refresh_token)
						commit('SET_USER', data.user)
						commit('SET_LOGGEDIN', true)
						return true
					}
					return false
				})
				.catch( error => {

          commit('LOGOUT')
					return error
				})

    }catch( error ) {

    }
  },

}

export const getters = {
  isLoggedIn: state => Boolean(state.isLoggedIn),
  authStatus: state => Boolean(state.status),
  user: state => state.user
}
