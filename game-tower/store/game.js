export const state = () => ({

})

export const mutations = {
}

export const actions = {
  async start({ commit, dispatch }, {payload}){
    try {
      const result = this.$axios.post('api/game/start', payload)
          .then( ({data})  => {
            return data.game
          })
				  .catch( (error) => {
            return error
          })
          return result
    }catch (e){
      console.log('catch' , e.response)
    }


  },
  async move({ commit, dispatch }, { payload }) {
		try {
      const result  = this.$axios.post('api/game/move', payload)
          .then( ({data})  => {
            return data.game
          })
				  .catch( (error) => {
            return error
          })
          return result
    }catch (e){
    }
  },
  async finish({ commit, dispatch }, {payload}) {
    try {
      const result  = this.$axios.post('api/game/finish', payload)
          .then( ({data})  => {
            return data.game
          })
				  .catch( (error) => {
            return error
          })
          return result
    }catch (e){
    }
  },
  async lastGame({commit, dispatch}){
    const result = this.$axios.get('api/profile/last-game')
      .then( ({data})  => {
        return  data.game
      })
      .catch( (error) => {
        console.log(error.response)
      })
      return result
  }

}

export const getters = {
}
