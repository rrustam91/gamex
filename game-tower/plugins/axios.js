export default function({ $axios, store, redirect }) {
  $axios.onRequest(request => {
    if (store.state.auth.isLoggedIn) {
      const token = store.state.auth.token;
      request.headers.common["Authorization"] = "Bearer " + token;
    }

    return request;
  });
  $axios.onResponseError(err => {
    if(err.response.status === 401){
      store.dispatch('auth/logout')
    }
    console.log('>>', err.response.status)

  });
}
