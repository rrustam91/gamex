const GameEndpoints = {
  tower: {
    start: '/api/game/start/',
    move: '/api/game/move/',
    finish: '/api/game/finish/',
    status: '/api/game/status/',
    history: '/api/game/history/',
  }

}
export default GameEndpoints
