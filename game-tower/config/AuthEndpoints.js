const AuthEndpoints = {
  login: '/api/auth/login/',
  registration: '/api/auth/register/',
  refresh: '/api/auth/refresh/',
  check: '/api/auth/check/',
}
export default AuthEndpoints
